import { Router } from 'express';
import { commandeService } from '../services/commande.service';
import { Commande, Product, Article, ConvertHttpRequestToProduct } from '../models/commande.model';
import { User } from '../models/user.model';
import { returnSuccess } from '../errors/success';
import { Token } from '../models/token.model';
import { Address } from '../models/address.model';

const commandeController = Router()
const commandeSvc = new commandeService()

commandeController.get('/get/:id_commande',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: parseInt(req.query.id_user),
            type: parseInt(req.query.type),
            token: req.headers.authorization,
        })

        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        const commande: Commande = new Commande({
            id_commande: req.params.id_commande
        })

        commande.IsIdCommandeValid()

        var commande_res = await commandeSvc.getCommandeByIdCommande(commande, user)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/user/:id_user', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: parseInt(req.params.id_user),
            type: parseInt(req.query.type),
        })

        user.IsIdUserValid()
        user.IsTypeValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        var commande_res = await commandeSvc.getCommandeByIdClient(user, user_token)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/get/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state
        })

        commande.IsCommandeStateValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const commande_res = await commandeSvc.getCommandeByCommandeState(commande, user_token)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/restaurateur/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state,
            id_restaurateur: parseInt(req.query.id_restaurateur)
        })

        commande.IsCommandeStateValid()
        commande.IsIdRestaurateurValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const commande_res = await commandeSvc.getCommandeByCommandeStateAndIdRestaurateur(commande, user_token)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/client/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state,
            id_client: parseInt(req.query.id_client),
        })

        commande.IsCommandeStateValid()
        commande.IsIdClientValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const commande_res = await commandeSvc.getCommandeByCommandeStateAndIdClient(commande, user_token)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/commande_state/delivery_driver/:state', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const commande: Commande = new Commande({
            commande_state: req.params.state,
            id_delivery_driver: parseInt(req.query.id_delivery_driver),
        })

        commande.IsCommandeStateValid()
        commande.IsIdDeliveryDriverValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const commande_res = await commandeSvc.getCommandeByCommandeStateAndIdDeliveryDriver(commande, user_token)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.get('/waiting', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const userToken: Token = new Token({
            token: req.headers.authorization,
            type: parseInt(req.query.type),
            id_user_token: req.query.id_user_token,
        })

        userToken.IsTypeValid()
        userToken.IsIdUserValid()
        userToken.IsTokenValid()

        userToken.token = String(userToken.token).replace("Bearer ", "");

        var commande_res = await commandeSvc.getCommandeWaiting(userToken)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const userToken: Token = new Token({
            token: req.headers.authorization,
            type: parseInt(req.body.type),
            id_user_token: req.body.id_user_token,
        })

        userToken.IsTypeValid()
        userToken.IsIdUserValid()
        userToken.IsTokenValid()

        userToken.token = String(userToken.token).replace("Bearer ", "");


        const commande: Commande = new Commande({
            id_client: req.body.id_client,
            id_restaurateur: req.body.id_restaurateur,
            client_email: req.body.client_email,
            client_name: req.body.client_name,
            client_first_name: req.body.client_first_name,
            restaurant_name: req.body.restaurant_name,
            commande_state: "waiting",
            price: req.body.price,
            products: ConvertHttpRequestToProduct(req.body.products),
            client_address: new Address({
                street: req.body.client_address.street,
                zip_code: req.body.client_address.zip_code,
                city: req.body.client_address.city,
            }),
            restaurant_address: new Address({
                street: req.body.restaurant_address.street,
                zip_code: req.body.restaurant_address.zip_code,
                city: req.body.restaurant_address.city,
            })
        })

        commande.IsIdClientValid()
        commande.IsIdRestaurateurValid()
        commande.IsClientEmailValid()
        commande.IsClientNameValid()
        commande.IsClientFirstNameValid()
        commande.IsRestaurantNameValid()
        commande.IsCommandeStateValid()
        commande.IsPriceValid()
        commande.client_address.IsAddressValid()
        commande.restaurant_address.IsAddressValid()

        for (let i = 0; i < commande.products.length; i++) {
            commande.products[i].IsProduceValid()
        }

        var commande_res = await commandeSvc.addCommande(commande, userToken)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

commandeController.put('/update/:id_commande', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const userToken: Token = new Token({
            token: req.headers.authorization,
            type: parseInt(req.body.type),
            id_user_token: req.body.id_user_token,
        })

        userToken.IsTypeValid()
        userToken.IsIdUserValid()
        userToken.IsTokenValid()

        userToken.token = String(userToken.token).replace("Bearer ", "");

        const commande: Commande = new Commande({
            id_commande: req.params.id_commande,
            id_delivery_driver: req.body.id_delivery_driver,
            delivery_driver_name: req.body.delivery_driver_name,
            delivery_driver_first_name: req.body.delivery_driver_first_name,
            commande_state: req.body.commande_state,
        })

        commande.IsIdCommandeValid()

        var commande_res = await commandeSvc.updateCommande(commande, userToken)

        returnSuccess(res, commande_res)

    } catch (err) {
        next(err)
    }
})

export { commandeController }