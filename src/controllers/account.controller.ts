import { Router } from 'express';
import { Address } from '../models/address.model';
import { User } from '../models/user.model';
import { Token } from '../models/token.model';
import { accountService } from '../services/account.service';
import { BankingCard } from '../models/banking_cards.model';
import { returnSuccess } from '../errors/success';
import { Banking_Details } from '../models/banking_details.model';

const accountController = Router();
const accountSvc = new accountService();

// Get user by id depending on his type
accountController.get('/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: parseInt(req.params.id_user),
            type: parseInt(req.query.type)
        })

        user.IsIdUserValid()
        user.IsTypeValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })
        console.log("es que je passe ici ?", user, user_token)

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const user_res: User = await accountSvc.GetUserById(user, user_token)

        returnSuccess(res, user_res)

    } catch (err) {
        next(err)
    }
});

// Get users list depending by type
accountController.get('/all/:type(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: parseInt(req.params.type),
        })
        
        user.IsTypeValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const user_res: User = await accountSvc.GetUsersByType(user, user_token)

        returnSuccess(res, user_res)

    } catch (err) {
        next(err)
    }
});

// Get address from a user by id_user depending on his type 
accountController.get('/address/:id_user(\\d+)',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: parseInt(req.params.id_user),
            type: parseInt(req.query.type),
        })

        user.IsIdUserValid()
        user.IsTypeValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const address_res: User = await accountSvc.GetAddressByIdUser(user, user_token)

        returnSuccess(res, address_res)

    } catch (err) {
        next(err)
    }
})

// Get banking card offuscated from a user by id_user depending on his type 
accountController.get('/banking_card/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: parseInt(req.params.id_user),
            type: parseInt(req.query.type),
        })

        user.IsIdUserValid()
        user.IsTypeValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const banking_card_res: User = await accountSvc.GetBankingCardByIdUser(user, user_token)

        returnSuccess(res, banking_card_res)

    } catch (err) {
        next(err)
    }
})

accountController.get('/banking_details/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: parseInt(req.params.id_user),
            type: parseInt(req.query.type),
        })

        user.IsIdUserValid()
        user.IsTypeValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const banking_card_res: User = await accountSvc.GetBankingDetailsByIdUser(user, user_token)

        returnSuccess(res, banking_card_res)

    } catch (err) {
        next(err)
    }
})

// Add user depending on his type
accountController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            email: req.body.email,
            password: req.body.password,
            name: req.body.name,
            first_name: req.body.first_name,
            type: req.body.type
        })

        user.IsEmailValid()
        user.IsPasswordValid()
        user.IsNameValid()
        user.IsFirstNameValid()
        user.IsTypeValid()

        user.HashPassword()

        await accountSvc.AddUser(user)
        
        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

// Add Address to a user by id_user depending on his type
accountController.post('/address/add',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const address: Address = new Address({
            street: req.body.street,
            zip_code: req.body.zip_code,
            city: req.body.city,
            id_user: req.body.id_user,
        })
        

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type,
        }) 

        address.IsStreetValid()
        address.IsZipCodeValid()
        address.IsCityValid()
        address.IsIdUserValid()

        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.AddAddressByUserId(address, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

// Add Banking card to a user by id_user depending on his type
accountController.post('/banking_card/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const banking_card: BankingCard = new BankingCard({
            name: req.body.name,
            card_number: req.body.card_number,
            security_code: req.body.security_code,
            expiration_date: req.body.expiration_date,
            id_user: req.body.id_user,
        })

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type,
        })
        console.log(req.body)

        banking_card.IsNameValid()
        banking_card.IsCardNumberValid()
        banking_card.IsSecurityCodeValid()
        banking_card.IsExpirationDateValid()
        banking_card.IsIdUserValid()

        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.AddBankingCardByUserId(banking_card, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.post('/banking_details/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const banking_details: Banking_Details = new Banking_Details({
            rib: req.body.rib,
            id_user: req.body.id_user,
        })

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type,
        })

        banking_details.IsRibValid()
        banking_details.IsIdUserValid()

        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.AddBankingDetailsByUserId(banking_details, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})


accountController.put('/address/update/street', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const address: Address = new Address({
            street: req.body.street,
            id_address: req.body.id_address,
        })

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type,
        })

        address.IsStreetValid()
        address.IsIdAddressValid()

        user.IsIdUserValid()
        user.IsTokenValid()
        user.IsTypeValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateStreetByAdressId(address, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/address/update/zip_code', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const address: Address = new Address({
            zip_code: req.body.zip_code,
            id_address: req.body.id_address,
        })

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type,
        })

        address.IsZipCodeValid()
        address.IsIdAddressValid()

        user.IsIdUserValid()
        user.IsTokenValid()
        user.IsTypeValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateZipCodeByAdressId(address, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/address/update/city', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const address: Address = new Address({
            city: req.body.city,
            id_address: req.body.id_address,
        })

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type,
        })

        address.IsCityValid()
        address.IsIdAddressValid()

        user.IsIdUserValid()
        user.IsTokenValid()
        user.IsTypeValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateCityByAdressId(address, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/update/email', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            email: req.body.email,
            type: req.body.type
        })

        user.IsEmailValid()
        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateEmailByUserId(user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/update/password', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            password: req.body.password,
            type: req.body.type
        })

        user.IsPasswordValid()
        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        user.HashPassword()
        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdatePasswordByUserId(user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/update/name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            name: req.body.name,
            type: req.body.type
        })

        user.IsNameValid()
        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");
        
        await accountSvc.UpdateNameByUserId(user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/update/first_name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            first_name: req.body.first_name,
            type: req.body.type
        })

        user.IsFirstNameValid()
        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateFirstNameByUserId(user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/update/suspended', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            suspended: req.body.suspended,
            type: req.body.type
        })
        
        user.IsSuspendedValid()
        user.IsIdUserValid()
        user.IsTypeValid()
        
        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await accountSvc.UpdateSuspendedByUserId(user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/update/parrain', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            id_parrain: req.body.id_parrain,
            type: req.body.type
        })

        user.IsIdParrainValid()
        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateIdParrainByUserId(user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.put('/banking_details/update/rib', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            type: req.body.type
        })

        const banking_details: Banking_Details = new Banking_Details({
            rib: req.body.rib,
        })

        user.IsIdUserValid()
        user.IsTypeValid()
        user.IsTokenValid()

        banking_details.IsRibValid()

        user.token = String(user.token).replace("Bearer ", "");

        await accountSvc.UpdateRibByUserId(banking_details, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.delete('/address/delete/:id_address(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: req.body.type
        })

        const address: Address = new Address({
            id_address: req.params.id_address
        })

        const user_token: User = new User({
            id_user: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user.IsTypeValid()

        address.IsIdAddressValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await accountSvc.DeleteAddressByAddressId(address, user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.delete('/address/delete/all/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: req.body.type,
            id_user: req.params.id_user
        })

        user.IsTypeValid()
        user.IsIdUserValid()

        const user_token: User = new User({
            id_user: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await accountSvc.DeleteAddressByUserId(user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.delete('/banking_card/delete/:id_banking_card(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: req.body.type
        })

        const banking_card: BankingCard = new BankingCard({
            id_banking_card: req.params.id_banking_card
        })

        user.IsTypeValid()
        
        banking_card.IsIdBankingCardValid()

        const user_token: User = new User({
            id_user: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");


        await accountSvc.DeleteBankingCardByBankingCardId(banking_card, user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.delete('/banking_card/delete/all/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: req.body.type,
            id_user: req.params.id_user
        })

        user.IsTypeValid()
        user.IsIdUserValid()

        const user_token: User = new User({
            id_user: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await accountSvc.DeleteBankingCardByUserId(user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.delete('/banking_details/delete/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: req.body.type,
            id_user: req.params.id_user
        })

        user.IsTypeValid()
        user.IsIdUserValid()

        const user_token: User = new User({
            id_user: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await accountSvc.DeleteBankingDetailsByUserId(user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

accountController.delete('/delete/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            type: req.body.type,
            id_user: req.params.id_user
        })

        user.IsTypeValid()
        user.IsIdUserValid()

        const user_token: User = new User({
            id_user: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        console.log(user_token);
        console.log(user);
        

        await accountSvc.DeleteAccountByUserId(user, user_token)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

export { accountController }
