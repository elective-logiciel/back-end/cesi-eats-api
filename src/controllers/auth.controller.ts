import { Router } from 'express';
import { User } from '../models/user.model';
import { authService } from '../services/auth.services';
import { returnSuccess } from '../errors/success';
import { Token } from '../models/token.model';

const authController = Router()
const authSvc = new authService()

authController.post('/sign_in', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User = new User({
            email: req.body.email,
            password: req.body.password
        })

        user.IsEmailValid()
        user.IsPasswordValid()

        user.HashPassword()

        const sign_in_res = await authSvc.SignIn(user)

        res.cookie("refresh_token", sign_in_res.refresh_token).status(201).json(sign_in_res);

    } catch (err) {
        next(err)
    }
})

authController.post('/refresh', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization,
            refresh_token: req.body.refresh_token,
        })

        user.IsIdUserValid()
        user.IsTokenValid()
        user.IsRefreshTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        const refresh_res = await authSvc.Refresh(user)

        res.cookie("refresh_token", refresh_res.refresh_token).status(200).json(refresh_res);

    } catch (err) {
        next(err)
    }
})

authController.post('/sign_out', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            id_user: req.body.id_user,
            token: req.headers.authorization
        })

        user.IsIdUserValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await authSvc.SignOut(user)

        res.clearCookie("refresh_token").status(200).json({
            status: 200,
            message: "Sign Out"
        });

    } catch (err) {
        next(err)
    }
})

authController.get('/verify', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: Token = new Token({
            id_user_token: parseInt(req.query.id_user),
            token: req.headers.authorization
        })

        user.IsIdUserValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        var token_res = await authSvc.Verify(user)

        returnSuccess(res, token_res)

    } catch (err) {
        next(err)
    }
})

authController.get('/verify_refresh', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: Token = new Token({
            id_user_token: parseInt(req.query.id_user),
            token: req.headers.authorization
        })

        user.IsIdUserValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        var token_res = await authSvc.VerifyRefresh(user)

        returnSuccess(res, token_res)

    } catch (err) {
        next(err)
    }
})

export { authController }