
import { Router } from 'express';
import { Article, Menu, RestaurantModel } from '../models/restaurant.model';
import { restaurantService } from '../services/restaurant.service';
import { User } from '../models/user.model';
import { returnSuccess } from '../errors/success';
import { Token } from '../models/token.model';

const restaurantController = Router();
const restaurantSvc = new restaurantService();

restaurantController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: req.body.id_restaurateur,
            restaurant_name: req.body.restaurant_name,
            restaurant_img: req.body.restaurant_img
        })

        const user: User = new User({
            id_user: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        restaurant.IsIdRestaurateurValid();
        restaurant.IsRestaurantNameValid();

        user.IsTypeValid()
        user.IsTokenValid()

        user.token = String(user.token).replace("Bearer ", "");

        await restaurantSvc.addRestaurant(restaurant, user)

        returnSuccess(res, null)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/all', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.query.type),
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const restaurants = await restaurantSvc.getAllRestaurants(user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/:id_restaurateur', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.query.type),
        })

        const restaurant: RestaurantModel = new RestaurantModel ({
            id_restaurateur: parseInt(req.params.id_restaurateur)
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        const restaurants = await restaurantSvc.getRestaurantById(restaurant, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.put('/update', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.body.type),
        })

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: parseInt(req.body.id_restaurateur),
            restaurant_name: req.body.restaurant_name,
            restaurant_img: req.body.restaurant_img
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        const restaurants = await restaurantSvc.updateRestaurant(restaurant, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.delete('/delete/:id_restaurateur', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.body.type),
        })

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: parseInt(req.params.id_restaurateur),
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        const restaurants = await restaurantSvc.deleteRestaurant(restaurant, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.post('/article/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.body.type),
        })

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: parseInt(req.body.id_restaurateur),
        })

        const article: Article = new Article({
            article_name: req.body.article_name,
            price: req.body.article_price,
            img: req.body.article_img,
            description: req.body.description
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        article.IsArticleNameValid()
        article.IsPriceValid()
        article.IsImageValid()
        article.IsDescriptionValid()

        const restaurants = await restaurantSvc.addArticle(restaurant, article, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/article/:id_article', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.query.type),
        })

        const article: Article = new Article({
            id_article: req.params.id_article
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        article.IsIdArticleValid()

        const restaurants = await restaurantSvc.getArticleByIdArticle(article, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/article/all/:id_restaurateur', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.query.type),
        })

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: req.params.id_restaurateur
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        const restaurants = await restaurantSvc.getArticlesByIdRestaurateur(restaurant, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.put('/article/update', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const article: Article = new Article({
            id_article: req.body.id_article,
            article_name: req.body.article_name,
            price: req.body.article_price,
            img: req.body.article_img,
            description: req.body.description
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        article.IsIdArticleValid()

        const restaurants = await restaurantSvc.updateArticle(article, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

// restaurantController.put('/article/update', async (req, res, next) => {
//     try {
//         console.log('Request send:', req.originalUrl)

//         const user_token: Token = new Token({
//             id_user_token: req.body.id_user_token,
//             token: req.headers.authorization,
//             type: req.body.type,
//         })

//         const article: Article = new Article({
//             id_article: req.body.id_article,
//             article_name: req.body.article_name,
//             price: req.body.article_price,
//             img: req.body.article_img,
//             description: req.body.article_description
//         })

//         user_token.IsTypeValid()
//         user_token.IsTokenValid()
//         user_token.IsIdUserValid()

//         user_token.token = String(user_token.token).replace("Bearer ", "");

//         article.IsIdArticleValid()

//         const restaurants = await restaurantSvc.updateArticle(article, user_token)

//         returnSuccess(res, restaurants)

//     } catch (err) {
//         next(err)
//     }
// })

restaurantController.delete('/article/delete/:id_article', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const article: Article = new Article({
            id_article: req.params.id_article
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        article.IsIdArticleValid()

        const restaurants = await restaurantSvc.deleteArticle(article, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.post('/menu/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: req.body.id_restaurateur,
            menus: req.body.menu
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        const restaurants = await restaurantSvc.addMenu(restaurant, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/menu/all/:id_restaurateur', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.query.type),
        })

        const restaurant: RestaurantModel = new RestaurantModel({
            id_restaurateur: req.params.id_restaurateur,
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        restaurant.IsIdRestaurateurValid()

        const restaurants = await restaurantSvc.getAllMenusByIdRestaurateur(restaurant, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/menu/:id_menu', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
            type: parseInt(req.query.type),
        })

        const menu: Menu = new Menu({
            id_menu: req.params.id_menu,
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        menu.IsIdMenuValid()

        const restaurants = await restaurantSvc.getMenuByIdMenu(menu, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.put('/menu/update', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const menu: Menu = new Menu({
            id_menu: req.body.id_menu,
            menu_name: req.body.menu_name,
            price: req.body.menu_price,
            img: req.body.menu_img,
            description: req.body.menu_description
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        menu.IsIdMenuValid()

        const restaurants = await restaurantSvc.updateMenu(menu, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.delete('/menu/delete/:id_menu', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const menu: Menu = new Menu({
            id_menu: req.params.id_menu
        })

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        menu.IsIdMenuValid()

        const restaurants = await restaurantSvc.deleteMenu(menu, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.post('/menu/article/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const article: Article = new Article({
            article_name: req.body.article_name,
            img: req.body.article_img,
            price: req.body.article_price,
            description: req.body.article_description,
        })

        const menu: Menu = new Menu({
            id_menu: req.body.id_menu
        })

        article.IsArticleNameValid()
        article.IsPriceValid()

        menu.IsIdMenuValid()

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const restaurants = await restaurantSvc.AddArticleToMenu(article, menu, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

restaurantController.delete('/menu/article/delete/:id_article', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
            type: req.body.type,
        })

        const article: Article = new Article({
            id_article: req.params.id_article,
        })

        article.IsIdArticleValid()

        user_token.IsTypeValid()
        user_token.IsTokenValid()
        user_token.IsIdUserValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const restaurants = await restaurantSvc.DeleteArticleFromMenu(article, user_token)

        returnSuccess(res, restaurants)

    } catch (err) {
        next(err)
    }
})

export { restaurantController }