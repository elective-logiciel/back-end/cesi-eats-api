import { Router } from 'express';
import { returnSuccess } from '../errors/success';
import { Token } from '../models/token.model';
import { Composant } from '../models/composant.model';
import { composantService } from '../services/composant.service';

const composantController = Router()
const composantSvc = new composantService()

composantController.get('/get/:id_composant', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            id_composant: req.params.id_composant
        })

        comp.IsIdComposantValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const comp_res = await composantSvc.GetComposantById(comp, user_token)

        returnSuccess(res, comp_res)
    } catch (err) {
        next(err)
    }
})

composantController.get('/all', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const comp_res = await composantSvc.GetAllComposants(user_token)

        returnSuccess(res, comp_res)
    } catch (err) {
        next(err)
    }
})

composantController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            name: req.body.name,
            url: req.body.url,
            description: req.body.description
        })

        comp.IsNameValid()
        comp.IsUrlValid()
        comp.IsDescriptionValid()

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await composantSvc.AddComposant(comp, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

composantController.put('/update', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            id_composant: req.body.id_composant,
            name: req.body.name,
            url: req.body.url,
            description: req.body.description
        })

        comp.IsIdComposantValid()

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await composantSvc.UpdateComposant(comp, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

composantController.delete('/delete', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const comp: Composant = new Composant({
            id_composant: req.body.id_composant,
        })

        comp.IsIdComposantValid()

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await composantSvc.DeleteComposant(comp, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

export { composantController }