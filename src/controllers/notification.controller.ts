import { Router } from 'express';
import { returnSuccess } from '../errors/success';
import { Token } from '../models/token.model';
import { notificationService } from '../services/notification.service';
import { Notification } from '../models/notification.model';

const notificationController = Router()
const notificationSvc = new notificationService()

notificationController.get('/get/:id_notification', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_notification: req.params.id_notification
        })

        notif.IsIdNotificationValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const notif_res = await notificationSvc.GetNotificationById(notif, user_token)

        returnSuccess(res, notif_res)

    } catch (err) {
        next(err)
    }
})

notificationController.get('/user/:id_user', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_user: req.params.id_user
        })

        notif.IsIdUserValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.query.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        const notif_res = await notificationSvc.GetNotificationsByIdUser(notif, user_token)

        returnSuccess(res, notif_res)

    } catch (err) {
        next(err)
    }
})

notificationController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_user: req.body.id_user,
            message: req.body.message,
            data: req.body.data,
        })

        notif.IsIdUserValid()
        notif.IsMessageValid()

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await notificationSvc.AddNotificationByIdUser(notif, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

notificationController.put('/view/:id_notification', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_notification: req.params.id_notification
        })

        notif.IsIdNotificationValid()

        const user_token: Token = new Token({
            id_user_token: req.body.id_user_token,
            token: req.headers.authorization,
        })        

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await notificationSvc.UpdateNotificationSeen(notif, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

notificationController.delete('/delete/:id_notification', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_notification: req.params.id_notification
        })

        notif.IsIdNotificationValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await notificationSvc.DeleteNotificationById(notif, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

notificationController.delete('/user/delete/', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_user: req.body.id_user
        })

        notif.IsIdUserValid()

        const user_token: Token = new Token({
            id_user_token: parseInt(req.body.id_user_token),
            token: req.headers.authorization,
        })

        user_token.IsIdUserValid()
        user_token.IsTokenValid()

        user_token.token = String(user_token.token).replace("Bearer ", "");

        await notificationSvc.DeleteNotificationByIdUser(notif, user_token)

        returnSuccess(res, null)
    } catch (err) {
        next(err)
    }
})

export { notificationController }