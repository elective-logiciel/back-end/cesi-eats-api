import { Router } from 'express';
import { loggingService } from '../services/logging.service';
import { returnSuccess } from '../errors/success';
import { Log } from '../models/log.model';
import { User } from '../models/user.model';

const loggingController = Router()
const loggingSvc = new loggingService()

loggingController.get('/level/info', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            type: parseInt(req.query.type)
        })

        user.IsTypeValid()

        var info = await loggingSvc.getInfo(user)

        returnSuccess(res, info)

    } catch (err) {
        next(err)
    }
})

loggingController.get('/level/warn', async (req, res, next) => {
    try {
        var user: User = new User({
            type: parseInt(req.query.type)
        })

        user.IsTypeValid()

        var warn = await loggingSvc.getWarn(user)

        returnSuccess(res, warn)

    } catch (err) {
        next(err)
    }
})

loggingController.get('/level/error', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            type: parseInt(req.query.type)
        })

        user.IsTypeValid()

        var error = await loggingSvc.getError(user)

        returnSuccess(res, error)

    } catch (err) {
        next(err)
    }
})

loggingController.get('/:service_name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var log: Log = new Log({
            service_name: req.params.service_name
        })

        var user: User = new User({
            type: parseInt(req.query.type)
        })

        user.IsTypeValid()

        var serviceLog = await loggingSvc.getByServiceName(log, user)

        returnSuccess(res, serviceLog)

    } catch (err) {
        next(err)
    }
})

loggingController.get('/level/all', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        var user: User = new User({
            type: parseInt(req.query.type)
        })

        user.IsTypeValid()
        console.log(user.type)

       var all = await loggingSvc.getAll(user)

        returnSuccess(res, all)

    } catch (err) {
        next(err)
    }
})

export { loggingController }