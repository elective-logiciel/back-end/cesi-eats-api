import { Article, Menu, RestaurantModel } from '../models/restaurant.model';
import { User } from '../models/user.model';
import { Error404 } from "../errors/errors";
import { loadBalancerService } from './loadbalancer.service';
import { redirectError } from '../errors/redirectError';
import { Token } from '../models/token.model';

const axios = require('axios');
const loadBalancerSvc = new loadBalancerService()

export class restaurantService {

    public async addRestaurant(restaurant: RestaurantModel, user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/add/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_restaurateur": restaurant.id_restaurateur,
                "restaurant_name": restaurant.restaurant_name,
                "restaurant_img": restaurant.restaurant_img,
                "id_user_token": user.id_user,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getAllRestaurants( user_token: Token ): Promise<any> {
        var url: string

        if (user_token.type === 4 || user_token.type === 5 || user_token.type === 6 || user_token.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/all'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getRestaurantById(restaurant: RestaurantModel, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4 || user_token.type === 5 || user_token.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/'+ restaurant.id_restaurateur

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async updateRestaurant(restaurant: RestaurantModel, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/update/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_restaurateur": restaurant.id_restaurateur,
                "restaurant_name": restaurant.restaurant_name,
                "restaurant_img": restaurant.restaurant_img
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async deleteRestaurant(restaurant: RestaurantModel, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/delete/'+ restaurant.id_restaurateur

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async addArticle(restaurant: RestaurantModel, article: Article, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/article/add/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_restaurateur": restaurant.id_restaurateur,
                "article": {
                    "article_name": article.article_name,
                    "price": article.price,
                    "img": article.img,
                    "description": article.description
                }
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async getArticleByIdArticle(article: Article, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4 || user_token.type === 5 || user_token.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/article/'+ article.id_article

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };
        var user_res = await axios.request(options)
        // console.log(user_res.data);

        return user_res.data
    }

    public async getArticlesByIdRestaurateur(restaurant: RestaurantModel, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4 || user_token.type === 5 || user_token.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/article/all/'+ restaurant.id_restaurateur

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async updateArticle(article: Article, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/article/update/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_article": article.id_article,
                "article_name": article.article_name,
                "img": article.img,
                "price": article.price,
                "description": article.description
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async deleteArticle(article: Article, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/article/delete/'+ article.id_article

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async addMenu(restaurant: RestaurantModel, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/add/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_restaurateur": restaurant.id_restaurateur,
                "menu": restaurant.menus
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async getAllMenusByIdRestaurateur(restaurant: RestaurantModel, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4 || user_token.type === 5 || user_token.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/all/'+ restaurant.id_restaurateur

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_restaurateur": restaurant.id_restaurateur,
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async getMenuByIdMenu(menu: Menu, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4 || user_token.type === 5 || user_token.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/'+ menu.id_menu

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async updateMenu(menu: Menu, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/update/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_menu": menu.id_menu,
                "menu_name": menu.menu_name,
                "img": menu.img,
                "price": menu.price,
                "description": menu.description
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async deleteMenu(menu: Menu, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/delete/' + menu.id_menu

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_menu": menu.id_menu
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async AddArticleToMenu(article: Article, menu: Menu, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/article/add/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_menu": menu.id_menu,
                "article_name": article.article_name,
                "price": article.price,
                "img": article.img,
                "description": article.description
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }

    public async DeleteArticleFromMenu(article: Article, user_token: Token): Promise<any> {
        var url: string

        if (user_token.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurant/menu/article/delete/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_article": article.id_article,
            }
        };
        var user_res = await axios.request(options)
        console.log(user_res.data);

        return user_res.data
    }
}