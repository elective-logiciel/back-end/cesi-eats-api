import { User } from "../models/user.model";
import { loadBalancerService } from './loadbalancer.service';
import { redirectError } from '../errors/redirectError';
import { Token } from "../models/token.model";

const axios = require('axios');
const loadBalancerSvc = new loadBalancerService()

export class authService {

    public async SignIn(user: User) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/auth/sign_in'

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
            },
            data: {
                "email": user.email,
                "password": user.password
            }
        };

        var payload = await axios.request(options)

        return payload.data
    }

    public async Refresh(user: User) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/auth/refresh'

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "refresh_token": user.refresh_token
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async SignOut(user: User) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/auth/sign_out'

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async Verify(user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/auth/verify'

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user_token.id_user_token,
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async VerifyRefresh(user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/auth/verify_refresh'

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user_token.id_user_token,
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }
}