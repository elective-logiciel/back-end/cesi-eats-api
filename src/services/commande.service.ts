import { Commande } from '../models/commande.model';
import { loadBalancerService } from './loadbalancer.service';
import { User } from '../models/user.model';
import { Error404 } from '../errors/errors';
import { Token } from '~/models/token.model';

const axios = require('axios');

const loadBalancerSvc = new loadBalancerService()

export class commandeService {

    public async getCommandeByIdCommande(commande: Commande, user: User) {

        var url: string

        if (user.type === 4 || user.type === 5 || user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/get/' + commande.id_commande
            //url = 'http://http://127.0.0.1:3000/restaurant/all'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user_token": user.id_user,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getCommandeByIdClient(user: User, userToken: Token) {
        var url: string


        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/restaurateur/' + user.id_user

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/delivery_driver/' + user.id_user

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/client/' + user.id_user
            //url = 'http://http://127.0.0.1:3000/restaurant/all'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_user_token": userToken.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getCommandeByCommandeState(commande: Commande, userToken: Token) {

        var url: string

        const server: string = loadBalancerSvc.GetServer1()
        url = 'http://' + server + '/commande/commande_state/get/' + commande.commande_state

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_user_token": userToken.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getCommandeByCommandeStateAndIdRestaurateur(commande: Commande, userToken: Token) {

        var url: string

        const server: string = loadBalancerSvc.GetServer1()
        url = 'http://' + server + '/commande/commande_state/restaurateur/' + commande.commande_state

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_restaurateur": commande.id_restaurateur,
                "id_user_token": userToken.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getCommandeByCommandeStateAndIdDeliveryDriver(commande: Commande, userToken: Token) {

        var url: string

        const server: string = loadBalancerSvc.GetServer1()
        url = 'http://' + server + '/commande/commande_state/delivery_driver/' + commande.commande_state

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_delivery_driver": commande.id_delivery_driver,
                "id_user_token": userToken.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getCommandeByCommandeStateAndIdClient(commande: Commande, userToken: Token) {

        var url: string

        const server: string = loadBalancerSvc.GetServer1()
        url = 'http://' + server + '/commande/commande_state/client/' + commande.commande_state

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_client": commande.id_client,
                "id_user_token": userToken.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async getCommandeWaiting(userToken: Token) {

        var url: string

        if (userToken.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/waiting'
            //url = 'http://http://127.0.0.1:3000/restaurant/all'

        } else {
            throw new Error404("Query only implemented for delivery drivers (5)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_user_token": userToken.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async addCommande(commande: Commande, userToken: Token) {

        var url: string

        if (userToken.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/add'
            //url = 'http://http://127.0.0.1:3000/restaurant/all'

        } else {
            throw new Error404("Query only implemented for clients (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_user_token": userToken.id_user_token,
                "id_client": commande.id_client,
                "id_restaurateur": commande.id_restaurateur,
                "client_email": commande.client_email,
                "client_name": commande.client_name,
                "client_first_name": commande.client_first_name,
                "restaurant_name": commande.restaurant_name,
                "price": commande.price,
                "products": commande.products,
                "client_address": commande.client_address,
                "restaurant_address": commande.restaurant_address
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async updateCommande(commande: Commande, userToken: Token) {

        var url: string

        if (userToken.type === 4 || userToken.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/commande/update/' + commande.id_commande
            //url = 'http://http://127.0.0.1:3000/restaurant/all'

        } else {
            throw new Error404("Query only implemented for restaurateurs (4) and delivery drivers (5)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + userToken.token
            },
            data: {
                "id_user_token": userToken.id_user_token,
                "id_delivery_driver": commande.id_delivery_driver,
                "delivery_driver_name": commande.delivery_driver_name,
                "delivery_driver_first_name": commande.delivery_driver_first_name,
                "commande_state": commande.commande_state
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }
}