import { Notification } from '../models/notification.model';
import { loadBalancerService } from './loadbalancer.service';
import { Token } from '../models/token.model';

const axios = require('axios');
const loadBalancerSvc = new loadBalancerService()

export class notificationService {
    public async GetNotificationById(notif: Notification, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/notification/get/' + notif.id_notification

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };

        var notif_res = await axios.request(options)

        return notif_res.data
    }

    public async GetNotificationsByIdUser(notif: Notification, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/notification/user/' + notif.id_user

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };

        var notif_res = await axios.request(options)

        return notif_res.data
    }

    public async AddNotificationByIdUser(notif: Notification, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/notification/add/'

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_user": notif.id_user,
                "message": notif.message,
                "data": notif.data
            }
        };

        await axios.request(options)
    }

    public async UpdateNotificationSeen(notif: Notification, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/notification/view/' + notif.id_notification

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };

        await axios.request(options)
    }


    public async DeleteNotificationById(notif: Notification, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/notification/delete/' + notif.id_notification

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };

        await axios.request(options)
    }

    public async DeleteNotificationByIdUser(notif: Notification, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/notification/user/delete/'

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_user": notif.id_user
            }
        };

        await axios.request(options)
    }
}