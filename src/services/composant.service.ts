import { Notification } from '../models/notification.model';
import { loadBalancerService } from './loadbalancer.service';
import { Token } from '../models/token.model';
import { Composant } from '../models/composant.model';

const axios = require('axios');
const loadBalancerSvc = new loadBalancerService()

export class composantService {
    public async GetComposantById(comp: Composant, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/composant/get/' + comp.id_composant

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };

        var comp_res = await axios.request(options)

        return comp_res.data
    }

    public async GetAllComposants(user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/composant/all/'

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };

        var comp_res = await axios.request(options)

        return comp_res.data
    }

    public async AddComposant(comp: Composant, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/composant/add/'

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "name": comp.name,
                "url": comp.url,
                "description": comp.description
            }
        };

        await axios.request(options)
    }

    public async UpdateComposant(comp: Composant, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/composant/update/'

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_composant": comp.id_composant,
                "name": comp.name,
                "url": comp.url,
                "description": comp.description
            }
        };

        await axios.request(options)
    }

    public async DeleteComposant(comp: Composant, user_token: Token) {
        const server: string = loadBalancerSvc.GetServer1()
        const url: string = 'http://' + server + '/composant/delete/'

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
                "id_composant": comp.id_composant,
            }
        };

        await axios.request(options)
    }
}