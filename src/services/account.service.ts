import { User } from "../models/user.model";
import { BankingCard, OffuscateBankingCard } from '../models/banking_cards.model';
import { Banking_Details } from '~/models/banking_details.model';
import { Address } from '../models/address.model';
import { Token } from "../models/token.model";
import { Error404 } from "../errors/errors";
import { loadBalancerService } from './loadbalancer.service';
import { redirectError } from '../errors/redirectError';

const axios = require('axios');
const loadBalancerSvc = new loadBalancerService()

export class accountService {

    public async GetUserById(user: User, user_token: Token): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) { 
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/' + user.id_user

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/' + user.id_user

        } else if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            // const server = "127.0.0.1:3000"
            url = 'http://' + server + '/client/' + user.id_user

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async GetUsersByType(user: User, user_token: Token): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/all'

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/all'

        } else if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/all'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async GetAddressByIdUser(user: User, user_token: Token): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/' + user.id_user

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/' + user.id_user

        } else if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/' + user.id_user

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token
            }
        };
        var address_res = await axios.request(options)

        return address_res.data
    }

    public async GetBankingCardByIdUser(user: User, user_token: Token): Promise<any> {
        var url: string
        
        if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/banking_card/' + user.id_user

        } else {
            throw new Error404("Query only implemented for client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token
            }
        };
        var banking_card_res = await axios.request(options)

        const banking_card_treated: BankingCard[] = OffuscateBankingCard(banking_card_res.data)
        
        banking_card_res.data.data = banking_card_treated
        
        return banking_card_res.data
    }

    public async GetBankingDetailsByIdUser(user: User, user_token: Token): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/banking_details/' + user.id_user

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/banking_details/' + user.id_user

        } else {
            throw new Error404("Query only implemented for reastaurateurs (4) and delivery drivers (5)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user_token
            }
        };
        var banking_details_res = await axios.request(options)
        
        return banking_details_res.data
    }

    public async AddUser(user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/add/'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/add/'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/add/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
            },
            data: {
                "email": user.email,
                "password": user.password,
                "name": user.name,
                "first_name": user.first_name,
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async AddAddressByUserId(address: Address, user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/add/'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/add/'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/add/'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "street": address.street,
                "zip_code": address.zip_code,
                "city": address.city,
                "id_user_token": user.id_user
            }
        };
        var address_res = await axios.request(options)

        return address_res.data
    }

    public async AddBankingCardByUserId(banking_card: BankingCard, user: User): Promise<any> {
        var url: string
        
        if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/banking_card/add/'

        } else {
            throw new Error404("Query only implemented for client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "name": banking_card.name,
                "card_number": banking_card.card_number,
                "security_code": banking_card.security_code,
                "expiration_date": banking_card.expiration_date,
                "id_user_token": user.id_user
            }
        };
        var banking_card_res = await axios.request(options)

        return banking_card_res.data
    }

    public async AddBankingDetailsByUserId(banking_details: Banking_Details, user: User): Promise<any> {
        var url: string
        console.log(banking_details);

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/banking_details/add/'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/banking_details/add'

        } else {
            throw new Error404("Query only implemented for reastaurateurs (4) and delivery drivers (5)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "rib": banking_details.rib,
                "id_user_token": user.id_user
            }
        };
        var banking_details_res = await axios.request(options)

        return banking_details_res.data
    }

    public async UpdateStreetByAdressId(address: Address, user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/update/street' 

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/update/street'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/update/street' 

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "id_address": address.id_address,
                "street": address.street,
                "id_user_token": user.id_user
            }
        };
        var address_res = await axios.request(options)

        return address_res.data
    }

    public async UpdateZipCodeByAdressId(address: Address, user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/update/zip_code'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/update/zip_code'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/update/zip_code'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "id_address": address.id_address,
                "zip_code": address.zip_code,
                "id_user_token": user.id_user
            }
        };
        var address_res = await axios.request(options)        

        return address_res.data
    }

    public async UpdateCityByAdressId(address: Address, user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/update/city'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/update/city'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/update/city'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "id_address": address.id_address,
                "city": address.city,
                "id_user_token": user.id_user
            }
        };
        var address_res = await axios.request(options)

        return address_res.data
    }

    public async UpdateEmailByUserId(user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/update/email'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/update/email'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/update/email'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "email": user.email,
                "id_user_token": user.id_user
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async UpdatePasswordByUserId(user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/update/password'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/update/password'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/update/password'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "password": user.password,
                "id_user_token": user.id_user
            }
        };
        var user_res = await axios.request(options)
        
        return user_res.data
    }

    public async UpdateNameByUserId(user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/update/name'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/update/name'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/update/name'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "name": user.name,
                "id_user_token": user.id_user
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async UpdateFirstNameByUserId(user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/update/first_name'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/update/first_name'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/update/first_name'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "first_name": user.first_name,
                "id_user_token": user.id_user
            }
        };
        var user_res = await axios.request(options)
        
        return user_res.data
    }

    public async UpdateSuspendedByUserId(user: User, user_token: Token): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/update/suspended'

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/update/suspended'

        } else if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/update/suspended'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user.id_user,
                "suspended": user.suspended,
                "id_user_token": user_token.id_user_token
            }
        };
        var user_res = await axios.request(options)
        
        return user_res.data
    }

    public async UpdateIdParrainByUserId(user: User): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/update/parrain'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/update/parrain'

        } else if (user.type === 6) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/update/parrain'

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5), and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "id_parrain": user.id_parrain,
                "id_user_token": user.id_user
            }
        };
        var user_res = await axios.request(options)

        return user_res.data
    }

    public async UpdateRibByUserId(banking_details: Banking_Details, user: User,): Promise<any> {
        var url: string

        if (user.type === 4) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/banking_details/update/rib'

        } else if (user.type === 5) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/banking_details/update/rib'

        } else {
            throw new Error404("Query only implemented for restaurateur (4) and delivery driver(5)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'PUT',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            data: {
                "id_user": user.id_user,
                "rib": banking_details.rib,
                "id_user_token": user.id_user
            }
        };
        var banking_details_res = await axios.request(options)

        return banking_details_res.data
    }

    public async DeleteAddressByAddressId(address: Address, user: User, user_token): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/delete/' + address.id_address

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/delete/' + address.id_address

        } else if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/delete/' + address.id_address

        } else {
            throw new Error404("Query only implemented for restaurateur (4),delivery driver (5) and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user_token": user_token.id_user,
                "id_address": address.id_address
            }
        };
        var address_res = await axios.request(options)
        
        return address_res.data
    }

    public async DeleteAddressByUserId(user: User, user_token: User): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/address/delete/all/' + user.id_user

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/address/delete/all/' + user.id_user

        } else if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/address/delete/all/' + user.id_user

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5) and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user.id_user,
                "id_user_token": user_token.id_user
            }
        };
        var address_res = await axios.request(options)
        
        return address_res.data
    }

    public async DeleteBankingCardByBankingCardId(banking_card: BankingCard, user: User, user_token: User): Promise<any> {
        var url: string

        if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/banking_card/delete/' + banking_card.id_banking_card

        } else {
            throw new Error404("Query only implemented for clients (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user.id_user,
                "id_banking_card": banking_card.id_banking_card,
                "id_user_token": user_token.id_user
            }
        };
        var banking_card_res = await axios.request(options)        

        return banking_card_res.data
    }

    public async DeleteBankingCardByUserId(user: User, user_token: User): Promise<any> {
        var url: string

        if (user.type === 6 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/banking_card/delete/all/' + user.id_user

        } else {
            throw new Error404("Query only implemented for clients (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user.id_user,
                "id_user_token": user_token.id_user
            }
        };
        var banking_card_res = await axios.request(options)        

        return banking_card_res.data
    }

    public async DeleteBankingDetailsByUserId(user: User, user_token: User): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/banking_details/delete/' + user.id_user

        } else if (user.type === 5 || user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/banking_details/delete/' + user.id_user

        } else {
            throw new Error404("Query only implemented for restaurateur (4) and delivery driver(5)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user.id_user,
                "id_user_token": user_token.id_user
            }
        };
        var banking_details_res = await axios.request(options)       
        
        return banking_details_res.data
    }

    public async DeleteAccountByUserId(user: User, user_token: User): Promise<any> {
        var url: string

        if (user.type === 4 || user.type === 3) {
            this.DeleteAddressByUserId(user, user_token)
            this.DeleteBankingDetailsByUserId(user, user_token)

            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/restaurateur/delete/' + user.id_user

        } else if (user.type === 5 || user.type === 3) {
            this.DeleteAddressByUserId(user, user_token)
            this.DeleteBankingDetailsByUserId(user, user_token)

            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/delivery_driver/delete/' + user.id_user

        } else if (user.type === 6 || user.type === 3) {
            this.DeleteAddressByUserId(user, user_token)
            this.DeleteBankingCardByUserId(user, user_token)

            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/client/delete/' + user.id_user

        } else {
            throw new Error404("Query only implemented for restaurateur (4), delivery driver(5) and client (6)")
        }

        console.log('Resquest send to: ' + url);

        const options = {
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user_token.token
            },
            data: {
                "id_user": user.id_user,
                "id_user_token": user_token.id_user
            }
        };
        var account_res = await axios.request(options)  

        return account_res.data
    }
}