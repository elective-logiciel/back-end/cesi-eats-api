import { Log } from "../models/log.model";
import { loadBalancerService } from './loadbalancer.service';
import { redirectError } from '../errors/redirectError';
import { Error404 } from "../errors/errors";
import { User } from "../models/user.model";

const axios = require('axios');
const loadBalancerSvc = new loadBalancerService()

export class loggingService {

    public async getInfo(user: User) {
        var url: string

        if (user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/logging/level/info'

        } else {
            throw new Error404("Query only implemented only for admins")
        }

        const options = {
            method: 'Get',
            url: url,
            headers: {
                'Content-Type': 'application/json',
            }
        };

        var payload = await axios.request(options)

        return payload.data
    }

    public async getWarn(user: User) {
        var url: string

        if (user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/logging/level/warn'
        } else {
            throw new Error404("Query only implemented only for admins")
        }

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async getError(user: User) {
        var url: string

        if (user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/logging/level/error'
        } else {
            throw new Error404("Query only implemented only for admins")
        }

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async getAll(user: User) {
        var url: string

        if (user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/logging/level/all'
        } else {
            throw new Error404("Query only implemented only for admins")
        }

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async getByServiceName(log: Log, user: User) {
        var url: string

        if (user.type === 3) {
            const server: string = loadBalancerSvc.GetServer1()
            url = 'http://' + server + '/logging/' + log.service_name
        } else {
            throw new Error404("Query only implemented only for admins")
        }

        const options = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        var payload = await axios.request(options)

        return payload.data
    }

    public async error(log: Log) {
        try {
            log.IsMessageValid()
            log.IsServiceNameValid()
            log.IsStatusNameValid()
            log.IsStatusValid()

            const server: string = loadBalancerSvc.GetServer1()
            const url: string = 'http://' + server + '/logging/error'

            const options = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    "message": log.message,
                    "status": log.status,
                    "status_name": log.status_name,
                    "service_name": log.service_name
                }
            };
            await axios.request(options)

        } catch (err) {
            console.log(err)
        }
    }

    public async info(log: Log) {
        try {
            log.IsMessageValid()
            log.IsServiceNameValid()
            log.IsStatusNameValid()
            log.IsStatusValid()

            const server: string = loadBalancerSvc.GetServer1()
            const url: string = 'http://' + server + '/logging/info'

            const options = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    "message": log.message,
                    "status": log.status,
                    "status_name": log.status_name,
                    "service_name": log.service_name
                }
            };
            await axios.request(options)

        } catch (err) {
            console.log(err)
        }
    }
}