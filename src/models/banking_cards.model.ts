import { Error400 } from "../errors/errors";

export class BankingCard {
    id_banking_card?: number
    name?: string
    card_number?: string
    security_code?: string
    expiration_date?: string
    id_user?: number

    public constructor(init?: Partial<BankingCard>) {
        Object.assign(this, init);
    }

    public IsIdBankingCardValid(): boolean {
        if (this.id_banking_card === undefined) {
            throw new Error400("Missing argument, 'id_banking_card' can not be NULL")
        }
        if (this.id_banking_card < 0) {
            throw new Error400("Out of range argument, 'id_banking_card' can not be a negative number")
        }
        return true
    }

    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsCardNumberValid(): boolean {
        if (this.card_number === undefined) {
            throw new Error400("Missing argument, 'card_number' can not be NULL")
        }
        if (this.card_number === "") {
            throw new Error400("Empty argument, 'card_number' can not be EMPTY")
        }
        if (this.card_number.length != 16) {
            throw new Error400("Invalid argument, 'card_number' must only have 16 caracter")
        }
        if (!(/^\d+$/.test(this.card_number))) {
            throw new Error400("Invalid argument, 'card_number' must only contain digit")
        }
        return true
    }

    public IsSecurityCodeValid(): boolean {
        if (this.security_code === undefined) {
            throw new Error400("Missing argument, 'security_code' can not be NULL")
        }
        if (this.security_code === "") {
            throw new Error400("Empty argument, 'security_code' can not be EMPTY")
        }
        if (this.security_code.length != 3) {
            throw new Error400("Invalid argument, 'security_code' must only have 3 caracter")
        }
        if (!(/^\d+$/.test(this.security_code))) {
            throw new Error400("Invalid argument, 'card_number' must only contain digit")
        }
        return true
    }

    public IsExpirationDateValid(): boolean {
        if (this.expiration_date === undefined) {
            throw new Error400("Missing argument, 'expiration_date' can not be NULL")
        }
        if (this.expiration_date === "") {
            throw new Error400("Empty argument, 'expiration_date' can not be EMPTY")
        } 
        if (this.expiration_date.length != 5) {
            throw new Error400("Invalid argument, 'expiration_date' must only have 5 caracter")
        }
        return true
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }
}

// Offuscate banking card
export function OffuscateBankingCard(query_res: any): BankingCard[] {
    let banking_card_list: BankingCard[] = []

    query_res = query_res.data
    
    for (let i = 0; i < query_res.length; i++) {
        let res_banking_card: any = query_res[i]

        const last4number: string = res_banking_card.card_number.slice(-4)

        banking_card_list.push(new BankingCard({
            id_banking_card: res_banking_card.id_banking_card,
            name: res_banking_card.name,
            card_number: "************" + last4number,
            expiration_date: res_banking_card.expiration_date,
        }))
    }

    return banking_card_list
}
