import { Error400, Error498 } from "../errors/errors"

var crypto = require('crypto'); 
var config = require('../config')

export class User {
    id_user?: number
    email?: string
    password?: string
    name?: string
    first_name?: string
    type?: number
    suspended?: boolean
    id_parrain?: number
    token?: string
    refresh_token?: string

    public constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }

    public IsEmailValid(): boolean {
        if (this.email === undefined) {
            throw new Error400("Missing argument, 'email' can not be NULL")
        }
        if (this.email === "") {
            throw new Error400("Empty argument, 'email' can not be EMPTY")
        }
        if (this.email.length > 50) {
            throw new Error400("Invalid argument, 'email' must only have 50 caracters")
        }
        return true
    }

    public IsPasswordValid(): boolean {
        if (this.password === undefined) {
            throw new Error400("Missing argument, 'password' can not be NULL")
        }
        if (this.password === "") {
            throw new Error400("Empty argument, 'password' can not be EMPTY")
        }
        return true
    }

    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsFirstNameValid(): boolean {
        if (this.first_name === undefined) {
            throw new Error400("Missing argument, 'first_name' can not be NULL")
        }
        if (this.first_name === "") {
            throw new Error400("Empty argument, 'first_name' can not be EMPTY")
        }
        return true
    }

    public IsTypeValid(): boolean {
        if (this.type === undefined) {
            throw new Error400("Missing argument, 'type' can not be NULL")
        }
        if (this.type < 0 || this.type > 6) {
            throw new Error400("Out of range argument, 'type' must be between [0;6]")
        }
        return true
    }

    public IsSuspendedValid(): boolean {
        if (this.suspended === undefined) {
            throw new Error400("Missing argument, 'first_name' can not be NULL")
        }
        return true
    }

    public IsIdParrainValid(): boolean {
        if (this.id_parrain === undefined) {
            throw new Error400("Missing argument, 'id_parrain' can not be NULL")
        }
        if (this.id_parrain < 0) {
            throw new Error400("Out of range argument, 'id_parrain' can not be a negative number")
        }
        return true
    }

    public IsTokenValid(): boolean {
        if (this.token === undefined) {
            throw new Error498("Missing argument, 'token' can not be NULL")
        }
        if (this.token === "") {
            throw new Error498("Empty argument, 'token' can not be EMPTY")
        }
        if (!this.token.startsWith('Bearer ')) {
            throw new Error498("Token Invalid")
        }
        return true
    }

    public IsRefreshTokenValid(): boolean {
        if (this.refresh_token === undefined) {
            throw new Error498("Missing argument, 'refresh_token' can not be NULL")
        }
        if (this.refresh_token === "") {
            throw new Error498("Empty argument, 'refresh_token' can not be EMPTY")
        }
        return true
    }

    public HashPassword() {

        const salt = config.hash.salt

        this.password = crypto.pbkdf2Sync(this.password, salt,
            1000, 64, `sha512`).toString(`hex`); 
    }
}
