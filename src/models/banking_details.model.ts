import { Error400 } from "../errors/errors"

export class Banking_Details {
    id_banking_details?: number
    rib?: string
    id_user?: number

    public constructor(init?: Partial<Banking_Details>) {
        Object.assign(this, init);
    }

    public IsIdABankingDetailsValid(): boolean {
        if (this.id_banking_details === undefined) {
            throw new Error400("Missing argument, 'id_banking_details' can not be NULL")
        }
        if (this.id_banking_details < 0) {
            throw new Error400("Out of range argument, 'id_banking_details' can not be a negative number")
        }
        return true
    }

    public IsRibValid(): boolean {
        if (this.rib === undefined) {
            throw new Error400("Missing argument, 'rib' can not be NULL")
        }
        if (this.rib === "") {
            throw new Error400("Empty argument, 'rib' can not be EMPTY")
        }
        if (this.rib.length != 27) {
            throw new Error400("Invalid argument, 'rib' must only have 27 caracter")
        }
        return true
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }
}