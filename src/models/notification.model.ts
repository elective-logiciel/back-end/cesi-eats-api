import { Error400 } from "../errors/errors";

export class Notification {
    id_notification: string
    id_user: number
    message: string
    time: Date
    new: boolean
    data: {}

    public constructor(init?: Partial<Notification>) {
        Object.assign(this, init);
    }

    public IsIdNotificationValid(): boolean {
        if (this.id_notification === undefined) {
            throw new Error400("Missing argument, 'id_notification' can not be NULL")
        }
        if (this.id_notification === "") {
            throw new Error400("Empty argument, 'id_notification' can not be EMPTY")
        }
        return true
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }

    public IsMessageValid(): boolean {
        if (this.message === undefined) {
            throw new Error400("Missing argument, 'message' can not be NULL")
        }
        if (this.message === "") {
            throw new Error400("Empty argument, 'message' can not be EMPTY")
        }
        return true
    }

    public IsNewNotifValid(): boolean {
        if (this.new === undefined) {
            throw new Error400("Missing argument, 'new' can not be NULL")
        }
        return true
    }
}