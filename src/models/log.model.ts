import { Error400, Error404, Error498 } from "../errors/errors"

export class Log {
    service_name: string 
    status: number 
    status_name: string 
    message: string

    public constructor(init?: Partial<Log>) {
        Object.assign(this, init);
    }

    public IsStatusValid(): boolean {
        if (this.status === undefined) {
            throw new Error400("Missing argument, 'status' can not be NULL")
        }
        if (this.status < 0) {
            throw new Error400("Out of range argument, 'status' can not be a negative number")
        }
        return true
    }

    public IsStatusNameValid(): boolean {
        if (this.status_name === undefined) {
            throw new Error400("Missing argument, 'status_name' can not be NULL")
        }
        if (this.status_name === "") {
            throw new Error400("Empty argument, 'status_name' can not be EMPTY")
        }
        return true
    }

    public IsMessageValid(): boolean {
        if (this.message === undefined) {
            throw new Error400("Missing argument, 'message' can not be NULL")
        }
        if (this.message === "") {
            throw new Error400("Empty argument, 'message' can not be EMPTY")
        }
        return true
    }

    public IsServiceNameValid(): boolean {
        if (this.service_name === undefined) {
            throw new Error400("Missing argument, 'serviceName' can not be NULL")
        }
        if (this.service_name === "") {
            throw new Error400("Empty argument, 'serviceName' can not be EMPTY")
        }
        return true
    }
}
