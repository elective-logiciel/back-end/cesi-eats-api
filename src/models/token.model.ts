import { Error400, Error498 } from "../errors/errors";

export class Token {
    id_user_token: number
    token: string
    type?: number

    public constructor(init?: Partial<Token>) {
        Object.assign(this, init);
    }

    public IsIdUserValid(): boolean {
        if (this.id_user_token === undefined) {
            throw new Error400("Missing argument, argument needed for token check, 'id_user_token' can not be NULL.")
        }
        if (this.id_user_token < 0) {
            throw new Error400("Out of range argument, argument needed for token check, 'id_user_token' can not be a negative number.")
        }
        return true
    }

    public IsTokenValid(): boolean {
        if (this.token === undefined) {
            throw new Error498("Missing argument, argument needed for token check, 'token' can not be NULL.")
        }
        if (this.token === "") {
            throw new Error498("Empty argument, argument needed for token check, 'token' can not be EMPTY.")
        }
        if (!this.token.startsWith('Bearer ')) {
            throw new Error498("Token Invalid, token need to be of type bearer.")
        }
        return true
    }

    public IsTypeValid(): boolean {
        if (this.type === undefined) {
            throw new Error400("Missing argument, 'type' can not be NULL")
        }
        if (this.type < 0 || this.type > 6) {
            throw new Error400("Out of range argument, 'type' must be between [0;6]")
        }
        return true
    }
}