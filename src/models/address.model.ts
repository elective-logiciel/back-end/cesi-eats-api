import { Error400 } from "../errors/errors"

export class Address {
    id_address?: number
    street?: string
    zip_code?: string
    city?: string
    id_user?: number

    public constructor(init?: Partial<Address>) {
        Object.assign(this, init);
    }

    public IsAddressValid(): boolean {
        this.IsStreetValid()
        this.IsZipCodeValid()
        this.IsCityValid()

        return true
    }

    public IsIdAddressValid(): boolean {
        if (this.id_address === undefined) {
            throw new Error400("Missing argument, 'id_address' can not be NULL")
        }
        if (this.id_address < 0) {
            throw new Error400("Out of range argument, 'id_address' can not be a negative number")
        }
        return true
    }

    public IsStreetValid(): boolean {
        if (this.street === undefined) {
            throw new Error400("Missing argument, 'street' can not be NULL")
        }
        if (this.street === "") {
            throw new Error400("Empty argument, 'street' can not be EMPTY")
        }
        return true
    }

    public IsZipCodeValid(): boolean {
        if (this.zip_code === undefined) {
            throw new Error400("Missing argument, 'zip_code' can not be NULL")
        }
        if (this.zip_code === "") {
            throw new Error400("Empty argument, 'zip_code' can not be EMPTY")
        }
        return true
    }

    public IsCityValid(): boolean {
        if (this.city === undefined) {
            throw new Error400("Missing argument, 'city' can not be NULL")
        }
        if (this.city === "") {
            throw new Error400("Empty argument, 'city' can not be EMPTY")
        }
        return true
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }
}


