import { Error400 } from "../errors/errors"

export class Article {
    id_article?: string
    article_name?: string
    price?: number
    img?: string | null
    description?: string | null

    public constructor(init?: Partial<Article>) {
        Object.assign(this, init);
    }

    public IsIdArticleValid(): boolean {
        if (this.id_article === undefined) {
            throw new Error400("Missing argument, 'id_article' can not be NULL")
        }
        if (this.id_article === "") {
            throw new Error400("Empty argument, 'id_article' can not be EMPTY")
        }
        return true
    }

    public IsArticleNameValid(): boolean {
        if (this.article_name === undefined) {
            throw new Error400("Missing argument, 'article_name' can not be NULL")
        }
        if (this.article_name === "") {
            throw new Error400("Empty argument, 'article_name' can not be EMPTY")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Argument, 'price' can not be negative")
        }
        return true
    }

    public IsImageValid(): boolean {
        if (this.img === undefined) {
            throw new Error400("Missing argument, 'img' can not be NULL")
        }
        if (this.img === "") {
            throw new Error400("Empty argument, 'img' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }
}

export class Menu {
    id_menu?: string
    menu_name?: string
    price?: number
    article?: Array<Article>
    img?: string | null
    description?: string | null

    public constructor(init?: Partial<Menu>) {
        Object.assign(this, init);
    }

    public IsIdMenuValid(): boolean {
        if (this.id_menu === undefined) {
            throw new Error400("Missing argument, 'id_menu' can not be NULL")
        }
        if (this.id_menu === "") {
            throw new Error400("Empty argument, 'id_menu' can not be EMPTY")
        }
        return true
    }

    public IsMenuNameValid(): boolean {
        if (this.menu_name === undefined) {
            throw new Error400("Missing argument, 'menu_name' can not be NULL")
        }
        if (this.menu_name === "") {
            throw new Error400("Empty argument, 'menu_name' can not be EMPTY")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Argument, 'price' can not be negative")
        }
        return true
    }

    public IsImageValid(): boolean {
        if (this.img === undefined) {
            throw new Error400("Missing argument, 'img' can not be NULL")
        }
        if (this.img === "") {
            throw new Error400("Empty argument, 'img' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }
}

export class RestaurantModel {
    id_restaurateur?: number
    restaurant_name?: string
    restaurant_img?: string
    articles?: Array<Article>
    menus?: Array<Menu>

    public constructor(init?: Partial<RestaurantModel>) {
        Object.assign(this, init);
    }

    public IsIdRestaurateurValid(): boolean {
        if (this.id_restaurateur === undefined) {
            throw new Error400("Missing argument, 'id_restaurateur' can not be NULL")
        }
        if (this.id_restaurateur < 0) {
            throw new Error400("Out of range argument, 'id_restaurateur' can not be a negative number")
        }
        return true
    }

    public IsRestaurantNameValid(): boolean {
        if (this.restaurant_name === undefined) {
            throw new Error400("Missing argument, 'restaurant_name' can not be NULL")
        }
        if (this.restaurant_name === "") {
            throw new Error400("Empty argument, 'restaurant_name' can not be EMPTY")
        }
        return true
    }
}