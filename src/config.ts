const assert = require("assert");
const dotenv = require("dotenv");

// read in the .env file
dotenv.config();

// capture the environment variables the application needs
const { 
    PORT,
    HOST,
    SVR1_ADDRESS,
    SVR1_PORT,
    SVR2_ADDRESS,
    SVR2_PORT,
    HASH_SALT,
    API_NAME
} = process.env;

// const sqlEncrypt = process.env.SQL_ENCRYPT === "true";

// validate the required configuration information
assert(PORT, "PORT configuration is required.");
assert(HOST, "HOST configuration is required.");
assert(SVR1_ADDRESS, "SVR1_ADDRESS configuration is required.");
assert(SVR1_PORT, "SVR1_PORT configuration is required.");
assert(SVR2_ADDRESS, "SVR2_ADDRESS configuration is required.");
assert(SVR2_PORT, "SVR2_PORT configuration is required.");
assert(HASH_SALT, "HASH_SALT configuration is required.");
assert(API_NAME, "API_NAME configuration is required.");

// export the configuration information
module.exports = {
    api_name: API_NAME,
    port: PORT,
    host: HOST,
    svr1: {
        address: SVR1_ADDRESS,
        port: SVR1_PORT,
    },
    svr2: {
        address: SVR2_ADDRESS,
        port: SVR2_PORT,
    },
    hash: {
        salt: HASH_SALT,
    }
};