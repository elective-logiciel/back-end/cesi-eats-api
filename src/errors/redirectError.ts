import { BaseError } from "./baseError"

// redirect error from micro-service
function redirectError(res) {
    if (res.status !== 200 && res.status !== 201) {
        throw new BaseError(
            'Microservice error: ' + res.message,
            res.status,
            res.name
        )
    }
}

export { redirectError }