function returnSuccess(res, data) {
    var returnData = null
    if (data !== null) {
        returnData = data.data
    }
    
    res.status(200).json({
        name: 'OK',
        status: 200,
        data: returnData
    })
}

export { returnSuccess }