const config = require('./config')
const app = require('./server')
import { Log } from './models/log.model'
import { loggingService } from "./services/logging.service"

const logSvc = new loggingService()

const { logError, returnError } = require('./errors/errorHandler')

/**
 * Lauching serveur
 */
app.listen(config.port, () => {
    console.log('Starting server listening on port', config.port)
    // var log: Log = new Log({
    //     service_name: config.api_name,
    //     status: 200,
    //     status_name: 'Connected',
    //     message: 'Starting server listening on port ' + config.port
    // })

    // logSvc.info(log)
})