import express from 'express'
import { accountController } from './controllers/account.controller'
import { authController } from './controllers/auth.controller'
import { commandeController } from './controllers/commande.controller'
import { Error404, Error408 } from './errors/errors'
import { restaurantController } from './controllers/restaurant.controller'
import { loggingController } from './controllers/logging.controller'
import { notificationController } from './controllers/notification.controller'
import { composantController } from './controllers/composant.controller'

const cors = require('cors')
const { logError, returnError } = require('./errors/errorHandler')
const config = require('./config')
// var timeout = require('connect-timeout');

/**
 * App creation
 */
const app = express()

/**
 * Deffining requets parsing
 */
app.use(express.json())
app.use(cors())

/**
 * Logger
 */

/**
 * Time-out definition
 */
// app.use(timeout(120000));
// app.use(haltOnTimedout);

// /**
//  * Authorization to connect to this api
//  */
// app.use(cors())

/**
 * Route handling
 */
app.use('/account', accountController)
app.use('/auth', authController)
app.use('/commande', commandeController)
app.use('/restaurant', restaurantController)
app.use('/logging', loggingController)
app.use('/notification', notificationController)
app.use('/composant', composantController)

// default route
app.use('*', (req, res, next) => {
    throw new Error404('Invalid route, "' + req.originalUrl + '" does not exist. If your are confidient in the your url, maibe the type of request is wrong (get, post, ...).')
})

/**
 *  Time-out handling
 */
// function haltOnTimedout(req, res, next){
//     if (req.timedout) {
//         next()
//     }
//     throw new Error408('Route "' + req.originalUrl + '" does not resolve in time.')
// }

/**
 * Root page
 */
// app.get('/', (req, res) => res.send('hello world'))

/**
 * Errors handling
 */
app.use(logError)
app.use(returnError)

/**
 * export application config
 */
module.exports = app;